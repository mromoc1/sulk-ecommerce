<?php 
    
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    function get_ws($data,$method,$type,$endpoint){//metodo para recibir la data correspondiente a cada endpoint
        $curl = curl_init();
        if($type=='live'){
            $TbkApiKeyId='597055555532';
            $TbkApiKeySecret='579B532A7440BB0C9079DED94D31EA1615BACEB56610332264630D42D0A36B1C';
            $url="https://webpay3g.transbank.cl".$endpoint;
        }else{
            $TbkApiKeyId='597055555532';
            $TbkApiKeySecret='579B532A7440BB0C9079DED94D31EA1615BACEB56610332264630D42D0A36B1C';
            $url="https://webpay3gint.transbank.cl".$endpoint;
        }
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => array(
            'Tbk-Api-Key-Id: '.$TbkApiKeyId.'',
            'Tbk-Api-Key-Secret: '.$TbkApiKeySecret.'',
            'Content-Type: application/json')
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return json_decode($response);
    }


    $total_boleta = $_GET["total_boleta"];
    $baseurl = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
    $action = isset($_GET["action"]) ? $_GET["action"] : 'init';
    $message=null;
    $post_array = false;
    $url_tbk = "https://localhost/sulk-ecommerce/web_vista_pago_norealizado.php";

    switch($action){
        case 'init': //INICIALIZACION DE LA TRANSACCION
            $message.= 'init';
            $buy_order = rand();
            $session_id = rand();
            $amount=$total_boleta;
            $return_url = $baseurl."?action=getResult";
            $type="test";
            $data='{
                "buy_order": "'.$buy_order.'",
                "session_id": "'.$session_id.'",
                "amount": '.$amount.',
                "return_url": "'.$return_url.'" 
            }';
            $method='POST';
            $endpoint='/rswebpaytransaction/api/webpay/v1.0/transactions'; //Genera la transaccion
            $response = get_ws($data,$method,$type,$endpoint);
            $message.= "<pre>";
            $message.= print_r($response,TRUE);
            $message.= "</pre>";
            $url_tbk = $response->url; //Genera un url
            $token = $response->token; //Genera un token que se invoca despues del primer llamado
            $submit='Continuar!';
        break;
        case "getResult":
            $message.= "<pre>".print_r($_POST,TRUE)."</pre>";
            if (!isset($_POST["token_ws"]))
                break;
            /** Token de la transacción */
            $token = filter_input(INPUT_POST, 'token_ws');
            $request = array("token" => filter_input(INPUT_POST, 'token_ws'));
            $data='';
            $method='PUT';
            $type='sandbox';
            $endpoint='/rswebpaytransaction/api/webpay/v1.0/transactions/'.$token;
            $response = get_ws($data,$method,$type,$endpoint);
            $message.= "<pre>";
            $message.= print_r($response,TRUE);
            $message.= "</pre>";
            $url_tbk = $baseurl."?action=getStatus";
            $submit='Ver Status!';
        break;
        case "getStatus":
            if (!isset($_POST["token_ws"]))break;
            /** Token de la transacción */
            $token = filter_input(INPUT_POST, 'token_ws');
            $request = array(
                "token" => filter_input(INPUT_POST, 'token_ws')
            );
            $data='';
            $method='GET';
            $type='sandbox';
            $endpoint='/rswebpaytransaction/api/webpay/v1.0/transactions/'.$token;
            $response = get_ws($data,$method,$type,$endpoint);
            $message.= "<pre>";
            $message.= print_r($response,TRUE);
            $message.= "</pre>";
            $submit='Volver a la pagina';
            $url_tbk = "https://localhost/sulk-ecommerce/web_vista_pago_exitoso.php";
        break;
        case "refund":
            if (!isset($_POST["token_ws"]))break;
            /** Token de la transacción */
            $token = filter_input(INPUT_POST, 'token_ws');
            $request = array("token" => filter_input(INPUT_POST, 'token_ws'));
            $amount=$total_boleta;
            $data='{"amount": '.$amount.'}';
            $method='POST';
            $type='sandbox';
            $endpoint='/rswebpaytransaction/api/webpay/v1.0/transactions/'.$token.'/refunds';
            $response = get_ws($data,$method,$type,$endpoint);
            $message.= "<pre>";
            $message.= print_r($response,TRUE);
            $message.= "</pre>";
            $submit='Volver a la pagina';
            $url_tbk = "https://localhost/sulk-ecommerce/web_vista_pago_exitoso.php";
        break;
    }
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Procesamiento de pago</title>
        <style>
            .container {height: 200px;position: relative;text-align: center;}
            .vertical-center {margin-top: 20%;}
            .lds-hourglass {display: inline-block;position: relative;width: 80px;height: 80px;}
            .lds-hourglass:after {content: " ";display: block;border-radius: 50%;width: 0;height: 0;margin: 8px;box-sizing: border-box;border: 32px solid purple;border-color: purple transparent purple transparent;animation: lds-hourglass 1.2s infinite;}
            @keyframes lds-hourglass {0% {transform: rotate(0);animation-timing-function: cubic-bezier(0.35, 0.035, 0.675, 0.19);}50% {transform: rotate(900deg);animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);}100% {transform: rotate(1800deg);}}
        </style>
    </head>
    <body>
        <div class="container">
            <div class="vertical-center">
                <div class="lds-hourglass"></div>
                <img src="./img/WebpayPlus_FB_300px.png">
                <p><?php echo $message; ?></p>
                <?php if (strlen($url_tbk)) { ?>
                <form name="brouterForm" id="brouterForm"  method="POST" action="<?=$url_tbk?>" style="display:block;">
                    <input type="hidden" name="token_ws" value="<?=$token?>" />
                    <input type="submit" value="<?=(($submit)? $submit : 'Cargando...')?>" style="border: 1px solid #6b196b;border-radius: 4px;background-color: #6b196b;color: #fff;font-family: Roboto,Arial,Helvetica,sans-serif;font-size: 1.14rem;font-weight: 500;margin: auto 0 0;padding: 12px;position: relative;text-align: center;-webkit-transition: .2s ease-in-out;transition: .2s ease-in-out;max-width: 200px;" />
                </form>
                <script>
            
                var auto_refresh = setInterval(
                function(){}, 15000);
                bmitform()
                {
                    //alert('test');
                    document.brouterForm.submit();
                }
                </script>
            <?php } ?>
            </div>
        </div>
    </body>
</html>
