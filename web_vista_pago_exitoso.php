<?php 
	include_once "menu_usuario.php";

?>
<link rel="stylesheet" href="./css/fontawesome-all.min.css">
<script src="./js/validar_sacar_carrito.js"></script>
<script type="text/javascript">
    function mostrar_fecha(){
        var cl = document.getElementById("fecha");
        cl.innerHTML = new Date();
    }
</script>
<script>
	function ValidarCantidad(){
		var cantidad_a_comprar = document.forms["fform"]["cantidad_a_sacar"].value;

		if(Number(cantidad_a_comprar) < 0){
			alert('No puede asignar valores negativos');
			return false;
		}
		return true;
	}
</script>


<div class="col-xs-12">
	<div id="fecha"></div>
	<br>
	<script type="text/javascript">mostrar_fecha();setInterval(mostrar_fecha, 1000);</script>	
	<h1>Compra Aprobada</h1> 
    Se ha enviado a su correo la boleta de la compra y la orden de compra.
</div>

